/**
 * 
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License.
 * Chatbot Técnico Telefonica Login With LDAP
 * Version: 1.0.0
 * Date: Enero 2020
 * Author: Everis IAChat
 * All rights reserved
 * 
 **/

export interface UserRequest extends User{
    password : string;
}

export interface User {
    username : string; 
    groups: Group[];
    domains : Domain[];
}

export interface Group{
    name : string;
}

export interface Domain{
    name : string;
}

export interface Response {
    code : number;
    data: any;
    message : string;
}

export interface UserGenerateToken {
    user : string;
    hostDestiny : string;
}