/**
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License.
 * Chatbot Técnico Telefonica
 * Version: 1.0.0
 * Date: Abril 2019
 * Author: everis Aric Gutierrez - Jose Rojas.
 * All rights reserved
 * @description Clase encargada de generar los reportes generados
 */
import cronFiles from 'node-cron';
const fs = require('fs');
const path = require('path');

export class CronTabExport {

    /**
     * @description Devuelve la instancia de la clase CronTab
     * @returns instancia de la clase
     */
    public static get(): CronTabExport {
        if (!CronTabExport.instance) {
            CronTabExport.instance = new CronTabExport();
        }
        return CronTabExport.instance;
    }

    private static ROUTE = {
        FILE_ERROR_OMMIT : 'chatbot-errors.log',
        LOG : '../../log' };
    private static instance: CronTabExport;

    private static mkDirByPathSync(targetDir: string, { isRelativeToScript = false } = {}): void {
        const sep = path.sep;
        const initDir = path.isAbsolute(targetDir) ? sep : '';
        const baseDir = isRelativeToScript ? __dirname : '.';
        return targetDir.split(sep).reduce((parentDir, childDir) => {
          const curDir = path.resolve(baseDir, parentDir, childDir);
          try {
            fs.mkdirSync(curDir);
          } catch (err) {
            if (err.code === 'EEXIST') { // curDir already exists!
              return curDir;
            }
            // To avoid `EISDIR` error on Mac and `EACCES`-->`ENOENT` and `EPERM` on Windows.
            if (err.code === 'ENOENT') { // Throw the original parentDir error on curDir `ENOENT` failure.
              throw new Error(`EACCES: permission denied, mkdir '${parentDir}'`);
            }
            const caughtErr = ['EACCES', 'EPERM', 'EISDIR'].indexOf(err.code) > -1;
            if (!caughtErr || caughtErr && curDir === path.resolve(targetDir)) {
              throw err; // Throw if it's just the last created dir.
            }
          }
          return curDir;
        }, initDir);
      }
    private constructor() { }

    /**
     * @description Inicia el servicio de Cron Tab
     */
    public runService(): void {
        CronTabExport.mkDirByPathSync(CronTabExport.ROUTE.LOG);
        // #                ┌──────────── minute
        // #                │  ┌────────── hour
        // #                │  │  ┌──────── day of month
        // #                │  │  │ ┌────── month
        // #                │  │  │ │ ┌──── day of week
        // #                │  │  │ │ │
        // #                │  │  │ │ │
        cronFiles.schedule('50 23 1 * *', async () => {
        // cronFiles.schedule('* * * * *', async () => {
            CronTabExport.get().clearLogs();
        }, { scheduled: true, timezone: 'America/Bogota' });
    }

    private clearLogs(): void {
        const dir = path.join( __dirname , CronTabExport.ROUTE.LOG);
        let date = new Date('1990-01-01');
        let nameFileOmit = '';
        fs.readdir( dir , (err: any, files: string[]) => {
            for (const file of files) {
                const dataNameFile = file.split('-');
                if (dataNameFile.length === 4){
                    let dateFile = new Date();
                    dateFile.setFullYear( Number(dataNameFile[0]) );
                    dateFile.setMonth( Number(dataNameFile[1]) );
                    dateFile.setDate( Number(dataNameFile[2]) );
                    if (date.getTime() < dateFile.getTime()) {
                        date = dateFile;
                        nameFileOmit = file;
                    }
                }
            }
            for (const file of files) {
                if (file.indexOf(nameFileOmit) === -1 && file.indexOf(CronTabExport.ROUTE.FILE_ERROR_OMMIT) === -1 ) {
                    const nameFileDelete = dir + '/' + file;
                    fs.unlink(nameFileDelete , (err: any) => { if (err) { return console.log(err); } });
                }
            }
        } );
    }

}
