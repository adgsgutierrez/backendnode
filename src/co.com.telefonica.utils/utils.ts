import { Response } from "../co.com.telefonica.interface/interfaces";
import { MESSAGES } from "./constants";

export class Utils {

    private static INTERATION_ENCRIPTION: number = 2;
    private static userAuthorization = 'userAuthorization';
    private static passwordAuthorization = 'passwordAuthorization';


    public static MESSAGE = {
        SUCCESS : 'SUCCESS',
        ERROR : 'ERROR',
        NO_AUTORIZATION : 'NO_AUTORIZATION',
        ERROR_PARAMETERS : 'ERROR_PARAMETERS'
    }

    public static getResponse(code : string , data?:any): Response {
        return {
            code : MESSAGES[code].CODE,
            data,
            message : MESSAGES[code].MESSAGE
        }
    }

    public static validateDateRequest(code: string): boolean {
        const dateNow = new Date( new Date().toLocaleString("es-ES", {timeZone: 'America/Bogota'}) );
        // const dateNow = new Date('2020-01-08');
        let returnValidation = false;
        const dateCodeString = code.split('$')[1];
        const dayCode = dateCodeString.substring(6,8);
        const monthCode = dateCodeString.substring(4,6);
        const yearCode = dateCodeString.substring(0,4);
        const dateCode = new Date(yearCode + '-' + monthCode + '-' + dayCode);
        dateCode.setDate(parseInt(dayCode));
        dateCode.setMonth(parseInt(monthCode) - 1 );
        dateCode.setFullYear(parseInt(yearCode));
        if (dateNow.getDate() === dateCode.getDate() && dateCode.getMonth() === dateNow.getMonth()) {
            returnValidation = true;
        }
        console.log('Date returnValidation '+ returnValidation);
        return returnValidation;
    }

    public static validateFieldRequest(code: string , field: string , env: any): boolean {
        const fieldCodeString = code.split('$')[0];
        const param = env[field];
        return ( fieldCodeString === param);
    }

    public static getObjectDataUser( response: any , usergroups: any[]): any{
        for (const group of usergroups){
            if ( response.groups.indexOf(group.cn) === -1 ) {
                response.groups.push(group.cn);
            }
        }
    }

    public static getDataTokenize( data: string ): string{
        let dataValidate: string =  Buffer.from( data , 'base64').toString();
        for (let index = 0 ; index < Utils.INTERATION_ENCRIPTION ; index ++) {
            dataValidate = Buffer.from( (dataValidate.substr( 1 , dataValidate.length )) , 'base64').toString();
        }
        dataValidate = decodeURI(dataValidate);
        return dataValidate;
    }

}