
/**
 * 
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License.
 * Chatbot Técnico Telefonica Login With LDAP
 * Version: 1.0.0
 * Date: Enero 2020
 * Author: Everis IAChat
 * All rights reserved
 * 
 **/

export const APP = {
    CONFIG_FOLDER : 'config',
    CONFIG_FILE : '.env',
    SSL_FOLDER : 'sslcert/',
    ROUTES : {
        LOGIN_SERVICE : '/api/v1/authorization', 
        LOGIN_LDAP : '/api/v1/authorization/ldap'
    }
}

export const MESSAGES = {
    SUCCESS : {
        CODE : 200,
        MESSAGE: 'Success'
    },
    ERROR : {
        CODE : 500,
        MESSAGE: 'Error Internal'
    },
    NO_AUTENTICATE : {
        CODE : 401,
        MESSAGE: 'Error in credentials'
    },
    NO_DATA : {
        CODE : 404,
        MESSAGE: 'Error groups not available'
    },
    NO_AUTORIZATION : {
        CODE : 401,
        MESSAGE : 'No authorization request'
    },
    ERROR_PARAMETERS : {
        CODE : 402,
        MESSAGE : 'No parameters values request'
    }
}

const date = new Date();

const today = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;

const fn = today + '-server.log';


export const CONFIGURE_LOG = {
    appenders: {
      everything: { type: 'file', filename: 'log/'+fn , maxLogSize: 5242880 }
    },
    categories: {
      default: { appenders: [ 'everything' ], level: 'debug' }
    }
  }