export let successResponse = {
    "code": 200,
    "data": {
        "domain": ["nh", "inet", "in"],
        "groups": ["GRUPOS NUEVOS", "Grupos", "Telecom", "Aplicaciones"],
        "directory": ["ToolBox", "CSC_TecnicoEECC", "TOADirect"],
        "none": []
    },
    "message": "Success"
};

export let shellResponse : string =
"# LDAPv3\n"+
"# base <DC=nh,DC=inet> with scope subtree\n"+
"# filter: sAMAccountName=YANTOLINE\n"+
"# requesting: memberOf\n"+
"#\n"+
"\n"+
"# Yonathan Antoline, Temporales, Usuarios, Oriente, nh.inet\n"+
"dn: CN=Yonathan Antoline,OU=Temporales,OU=Usuarios,OU=Oriente,DC=nh,DC=inet\n"+
"memberOf: CN=ToolBox,OU=GRUPOS NUEVOS,OU=Grupos,OU=Telecom,DC=nh,DC=inet\n"+
"memberOf: CN=CSC_TecnicoEECC,OU=GRUPOS NUEVOS,OU=Grupos,OU=Telecom,DC=nh,DC=inet\n"+
"memberOf: CN=TOADirect,OU=Grupos,OU=Aplicaciones,OU=Telecom,DC=nh,DC=inet\n"+
"\n"+
"# search reference\n"+
"ref: ldap://DomainDnsZones.nh.inet/DC=DomainDnsZones,DC=nh,DC=inet\n"+
"\n"+
"# search reference\n"+
"ref: ldap://ForestDnsZones.nh.inet/DC=ForestDnsZones,DC=nh,DC=inet\n"+
"\n"+
"# search reference\n"+
"ref: ldap://nh.inet/CN=Configuration,DC=nh,DC=inet\n"+
"\n"+
"# search result\n"+
"search: 2\n"+
"result: 0 Success\n"+
"\n"+
"# numResponses: 5\n"+
"# numEntries: 1\n"+
"# numReferences: 3\n";

export let dataResponse = {
    "domain": ["nh", "inet", "in"],
    "groups": ["GRUPOS NUEVOS", "Grupos", "Telecom", "Aplicaciones"],
    "directory": ["ToolBox", "CSC_TecnicoEECC", "TOADirect"],
    "none": []
};

export let arrayLista = [
    'ref: ldap://DomainDnsZones.nh.inet/DC=DomainDnsZones,DC=nh,DC=inet',
    '',
    '# search reference',
    'ref: ldap://ForestDnsZones.nh.inet/DC=ForestDnsZones,DC=nh,DC=inet',
    '',
    '# search reference',
    'ref: ldap://nh.inet/CN=Configuration,DC=nh,DC=inet',
    '',
    '# search result',
    'search: 2',
    'result: 0 Success',
    '',
    '# numResponses: 5',
    '# numEntries: 1',
    '# numReferences: 3',
    ''];


export let items = ['CN=ToolBox',
    'OU=GRUPOS NUEVOS',
    'OU=Grupos',
    'OU=Telecom',
    'DC=nh',
    'DC=inet'];