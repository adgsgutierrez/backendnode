import { CronTabExport } from './co.com.telefonica.utils/export.controller';
/**
 * 
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License.
 * Chatbot Técnico Telefonica Login With LDAP
 * Version: 1.0.0
 * Date: Enero 2020
 * Author: Everis IAChat
 * All rights reserved
 * 
 **/

import { config } from 'dotenv';
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import * as path from 'path';
import { APP } from './co.com.telefonica.utils/constants';
import { Authorization } from './co.com.telefonica.ldap/authorization';
import http from 'http';
import https from 'https';
import fs from 'fs';
import { Middleware } from './co.com.telefonica.ldap/middleware';

export class Server {

    private static run: Server;
    private server: express.Application;

    public static getInstance(): Server {
        if (!Server.run) {
            Server.run = new Server();
        }
        return Server.run;
    }

    public getServer(){
        return this.server;
    }

    private constructor() {
    }

    public async start(): Promise<void>{
        try {
            await this.loadServerWorkspace();
            await this.loadServerInit();
            await this.loadServerRoutes();
            CronTabExport.get().runService();
            this.loadServerListen();
        } catch (e) {
            console.error('Erno in start server (500)');
        }
    }

    private async loadServerInit(): Promise<void>{
        return new Promise(
            (resolve , reject) => {
                try {
                    this.server = express();
                    this.server.use(bodyParser.json());
                    this.server.use(bodyParser.urlencoded({ extended: false }));
                    this.server.use(cors());
                    this.server.use(express.static(path.join(__dirname, 'public')));
                    this.server.use((req, res, next) => {
                        res.setHeader('Access-Control-Allow-Origin', '*');
                        res.setHeader('Access-Control-Allow-Headers', 'bundle , Content-Type , token ');
                        res.setHeader('Access-Control-Allow-Methods', 'POST');
                        next();
                    });
                    resolve();
                } catch (e) {
                    reject('Erno in load server (502)');
                }
            }
        );
    }

    private async loadCertificate(): Promise<any> {
        const ENV_FILE = path.join(__dirname, APP.CONFIG_FOLDER , '/' , APP.SSL_FOLDER);
        const key = ENV_FILE + process.env.sslFileKey;
        const crt = ENV_FILE + process.env.sslFileCrt;
        var privateKey  = fs.readFileSync( key , 'utf8');
        var certificate = fs.readFileSync( crt , 'utf8');
        return {key: privateKey, cert: certificate};
    }

    private async loadServerRoutes(){
        try {
            // this.server.post( APP.ROUTES.LOGIN_LDAP, Security.getInstance().getMiddleware , this.login);
            this.server.post( APP.ROUTES.LOGIN_LDAP , Middleware.getMiddleware ,  this.login);
            //this.server.post( APP.ROUTES.LOGIN_LDAP, this.login );
        } catch (e) {
            console.error('Erno in load server (503)');
        }
    }

    private async login (req: express.Request, response: express.Response ) {
        const responsePeticion = await Authorization.getInstance().login(req.body);
        if (responsePeticion.message.includes('Error Internal')){
            responsePeticion.data = {"Error": "Error in access credentials"};  
        }
        const responseData = (responsePeticion.code === 200)? '' : ' Error : ' + JSON.stringify(responsePeticion.data);
        console.log('Response: Code ['+ responsePeticion.code +'] msm ['+ responsePeticion.message + '] ' + responseData);
        return response.status(responsePeticion.code).json(responsePeticion);  
    }

    private async loadServerListen(): Promise<void> {
        if(!module.parent){
            const portSSL = process.env.port || 3001;
            const port = process.env.portSSL || 3000;
            const credentials = await this.loadCertificate();
            var httpServer = http.createServer(this.server);
            var httpsServer = https.createServer(credentials, this.server);

            httpServer.listen(port);
            httpsServer.listen(portSSL);
            
            // this.server.listen( port , () => {
            //     logger.info('Server active in port :' + port);
            // });
        }
    }

    private async loadServerWorkspace(): Promise<void>{
        return new Promise(
            (resolve , reject) => {
                try {
                    const ENV_FILE = path.join(__dirname, APP.CONFIG_FOLDER , '/' , APP.CONFIG_FILE);
                    config({ path: ENV_FILE });
                    resolve();
                } catch (e) {
                    reject('Error in load workspace (501)');
                }
            }
        );
    }
}

Server.getInstance().start();