import { Server } from '../index';
import { Authorization } from './authorization';
import { successResponse, items, shellResponse, dataResponse } from '../mocks';

const shellMatchers = require('jest-shell-matchers');

let app = null;
const dateNow = new Date();
const month = dateNow.getMonth() + 1;
const monthNew = (month < 10) ? ('0' + month) : month;
let day = dateNow.getDate();
const dayNew = (day < 10) ? ('0' + day) : day;
let user = 'us3rL0g1n4uth$' + dateNow.getFullYear() + '' + monthNew + '' + dayNew;
let psd = 'p4ssw0rdL0g1n4uth$' + dateNow.getFullYear() + '' + monthNew + '' + dayNew;
let url = 'http://localhost:3001/api/v1/authorization/ldap';
let token = '';

describe("Test autorization ", () => {
    let spyRequest: jest.SpyInstance<Promise<any>>;
    // let spyseparatorCharacters: jest.SpyInstance<any>;
    const authorization = Authorization.getInstance();
    const userInput = {
        username: 'YANTOLINE',
        password: 'FT20CUs3_TLF18p$',
        groups: [],
        domains: []
    }
    console.log('[ UserImput ]: \n', userInput);
    
    const lista = [
        'ref: ldap://DomainDnsZones.nh.inet/DC=DomainDnsZones,DC=nh,DC=inet',
        '',
        '# search reference',
        'ref: ldap://ForestDnsZones.nh.inet/DC=ForestDnsZones,DC=nh,DC=inet',
        '',
        '# search reference',
        'ref: ldap://nh.inet/CN=Configuration,DC=nh,DC=inet',
        '',
        '# search result',
        'search: 2',
        'result: 0 Success',
        '',
        '# numResponses: 5',
        '# numEntries: 1',
        '# numReferences: 3',
        ''];
        console.log('[ Lista ]:\n', lista);
        

    beforeEach(() => {
        const appServer = Server.getInstance();
        appServer.start();
        app = appServer.getServer();
        user = Buffer.from(user).toString('base64');
        psd = Buffer.from(psd).toString('base64');
        url = Buffer.from(url).toString('base64');
        for (let index = 0; index < 2; index++) {
            const random = 66;
            user = Buffer.from((String.fromCharCode(random) + user)).toString('base64');
            psd = Buffer.from((String.fromCharCode(random) + psd)).toString('base64');
            url = Buffer.from((String.fromCharCode(random) + url)).toString('base64');
        };
        shellMatchers();
    })

    it("Login LDAPSERVICE", async () => {
        const userInput = {
            username: 'YANTOLINE',
            password: 'FT20CUs3_TLF18p$',
            groups: [],
            domains: []
        }

        spyRequest = jest.spyOn(authorization, 'login');
        spyRequest.mockImplementationOnce(() => new Promise((resolve) => { resolve(res); }));
        const response = await authorization.login(userInput);
        expect(response.code).toEqual(200);
    })



    afterEach(() => {
        jest.restoreAllMocks();
    });

})