import { UserRequest } from './../co.com.telefonica.interface/interfaces';
import { Response } from '../co.com.telefonica.interface/interfaces';
import { Utils } from '../co.com.telefonica.utils/utils';

export class Middleware{

    public static getMiddleware(req: any , res: any, next: any){
        let response: Response = Utils.getResponse('ERROR');
        try {
                let user: UserRequest = req.body;
                user.username = Utils.getDataTokenize(user.username);
                user.password = Utils.getDataTokenize(user.password);
                if (user.username !== '' && user.password !== '') {
                    req.body = user;
                    next();
                } else {
                    response = Utils.getResponse(Utils.MESSAGE.ERROR_PARAMETERS , null);
                    return res.status(response.code).json(response);
                }
        } catch (e) {
            console.error('Error in middleware :' + JSON.stringify(e));
            response = Utils.getResponse(Utils.MESSAGE.NO_AUTORIZATION , null);
            return res.status(response.code).json(response);
        }
    }

}