import { UserRequest, Response } from '../co.com.telefonica.interface/interfaces';
import { MESSAGES } from '../co.com.telefonica.utils/constants';
import ActiveDirectory from 'activedirectory2';

/**
 * 
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License.
 * Chatbot Técnico Telefonica Login With LDAP
 * Version: 1.0.0
 * Date: Enero 2020
 * Author: Everis IAChat
 * All rights reserved
 * 
 **/
export class Authorization {

	private static run: Authorization;

	private static activeDirectory: ActiveDirectory;
	private domainAccount = '';
	private configureLdapConnection: any = { 
		url: '',
		baseDN: '',
		includeMembership : 'all',
		attributes: {
			// memberOf name employeeNumber company department
			user: [ 'name', 'memberOf' , 'company', 'department' ]
		},
		filter : ''
	}


	private constructor(){

	}

	public static getInstance(): Authorization {
		if (!Authorization.run) {
			Authorization.run = new Authorization();	
		}
		return Authorization.run;
	}

	public async login(userInput: UserRequest): Promise<Response> {
		console.log('Request for user : [' + userInput.username +']');

		this.configureLdapConnection.url = process.env.ldapUrl;
		this.configureLdapConnection.baseDN = process.env.ldapBaseDn;
		this.domainAccount = process.env.ldapDomain;

		let response: Response = {
			code: MESSAGES.ERROR.CODE,
			data: null,
			message: MESSAGES.ERROR.MESSAGE
		}
		try {
			const data = await this.ldapLogin(userInput);
			response.data = data;
			response.code = MESSAGES.SUCCESS.CODE;
			response.message = MESSAGES.SUCCESS.MESSAGE;

		} catch (error) {
			if ( error === 301 ) {
				response.code = MESSAGES.NO_AUTORIZATION.CODE;
				response.message = MESSAGES.NO_AUTORIZATION.MESSAGE;
				response.data = error;
			} else if ( error === 302 ) {
				response.code = MESSAGES.NO_DATA.CODE;
				response.message = MESSAGES.NO_DATA.MESSAGE;
				response.data = error;
			} else {
				response.code = MESSAGES.NO_AUTORIZATION.CODE;
				response.message = MESSAGES.NO_AUTORIZATION.MESSAGE;
				response.data = error;
			}
		}

		return response;
	}

	public async ldapLogin(userInput: UserRequest): Promise<any> {
		return new Promise( (resolve , reject) => {
			this.configureLdapConnection.filter = `sAMAccountName=${userInput.username}`;
			Authorization.activeDirectory = new ActiveDirectory(this.configureLdapConnection);
			const sAMAccountName = userInput.username + this.domainAccount;
			Authorization.activeDirectory.authenticate( sAMAccountName , userInput.password, async (err, auth) => {
				try{
					if (err) {
						reject(err) ;
					} else {
						if (auth){
							const ldapData = await this.ldapSearchGroup(userInput );
							resolve( ldapData );
						} else {
							reject(301) ;
						}
					}
				} catch(error){
                    console.log('errorldaplogin',error);
                    reject(301);
                }
			});
		});
	}

	private async ldapSearchGroup(userInput: UserRequest): Promise<any>{
		return new Promise( (resolve , reject) => {
			const sAMAccountName = userInput.username + this.domainAccount;
			this.configureLdapConnection['username'] = sAMAccountName;
			this.configureLdapConnection['password'] = userInput.password;
			Authorization.activeDirectory = new ActiveDirectory(this.configureLdapConnection);
			Authorization.activeDirectory.getGroupMembershipForUser(sAMAccountName, async (err, groups) => {
				try{
					if (err) {
						reject(err) ;
					} else {
						if (!groups) { 
							reject(302) ;
						} else {
							let response = {
								groups : [], 
								user : {}
							};
							for (const group of groups) {
								if ( response.groups.indexOf(group['cn']) < 0 ) {
									response.groups.push( group['cn'] );
								}
							}
							response.user = await this.ldapSearchUser(userInput);
							resolve( response );
						}
					}
				}catch(error){
                    console.log('error search', error);
                    reject(error);
                }
			});
		});
	}

	private async ldapSearchUser(userInput: UserRequest): Promise<any>{
		return new Promise( (resolve , reject)=> {
			const sAMAccountName = userInput.username + this.domainAccount;
			this.configureLdapConnection['username'] = sAMAccountName;
			this.configureLdapConnection['password'] = userInput.password;
			Authorization.activeDirectory = new ActiveDirectory(this.configureLdapConnection);
			Authorization.activeDirectory.findUser(sAMAccountName ,( err , results)=>{
				try{
					if (err) {
						reject(err) ;
					} else {
						resolve(results);
					}
				}
                catch(error){
                    console.log('error search', error);
                    reject(error);
                }
			});
		});
	}
}