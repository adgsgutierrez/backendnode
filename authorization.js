"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("./../co.com.telefonica.utils/constants");
const constants_2 = require("../co.com.telefonica.utils/constants");
const activedirectory2_1 = __importDefault(require("activedirectory2"));
const log4js = __importStar(require("log4js"));
log4js.configure(constants_1.CONFIGURE_LOG);
const logger = log4js.getLogger();
/**
 *
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License.
 * Chatbot Técnico Telefonica Login With LDAP
 * Version: 1.0.0
 * Date: Enero 2020
 * Author: Everis IAChat
 * All rights reserved
 *
 **/
//const pino = require('pino')
//const log = pino({
//    prettyPrint: true,
//  level: 'trace'})
class Authorization {
    constructor() {
        this.domainAccount = '';
        this.configureLdapConnection = {
            url: '',
            baseDN: '',
            includeMembership: 'all',
            attributes: {
                // memberOf name employeeNumber company department
                //user: ['mail', 'name', 'company', 'department']
                user: ['name', 'memberOf', 'company', 'department']
            },
            filter: ''
 //           logging: log
        };
    }
    static getInstance() {
        if (!Authorization.run) {
            Authorization.run = new Authorization();
        }
        return Authorization.run;
    }
    login(userInput) {
        return __awaiter(this, void 0, void 0, function* () {
            logger.debug('Request for user : [' + userInput.username + ']');
            this.configureLdapConnection.url = process.env.ldapUrl;
            this.configureLdapConnection.baseDN = process.env.ldapBaseDn;
            this.domainAccount = process.env.ldapDomain;
            let response = {
                code: constants_2.MESSAGES.ERROR.CODE,
                data: null,
                message: constants_2.MESSAGES.ERROR.MESSAGE
            };
            try {
                const data = yield this.ldapLogin(userInput,(err, results) => {
                    if (err) {
                        reject(err);
                    }
                    else {
                        resolve(results);
                    }
                });
                response.data = data;
                response.code = constants_2.MESSAGES.SUCCESS.CODE;
                response.message = constants_2.MESSAGES.SUCCESS.MESSAGE;
            }
            catch (error) {
                if (error === 301) {
                    response.code = constants_2.MESSAGES.NO_AUTORIZATION.CODE;
                    response.message = constants_2.MESSAGES.NO_AUTORIZATION.MESSAGE;
                    response.data = error;
                }
                else if (error === 302) {
                    response.code = constants_2.MESSAGES.NO_DATA.CODE;
                    response.message = constants_2.MESSAGES.NO_DATA.MESSAGE;
                    response.data = error;
                }
                else {
                    response.code = constants_2.MESSAGES.NO_AUTORIZATION.CODE;
                    response.message = constants_2.MESSAGES.NO_AUTORIZATION.MESSAGE;
                    //logger.error('Error in try', error);
                    response.data = error;
                }
            }
            return response;
        });
    }
    ldapLogin(userInput) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                this.configureLdapConnection.filter = `sAMAccountName=${userInput.username}`;
                Authorization.activeDirectory = new activedirectory2_1.default(this.configureLdapConnection);
                const sAMAccountName = userInput.username + this.domainAccount;
                Authorization.activeDirectory.authenticate(sAMAccountName, userInput.password, (err, auth) => __awaiter(this, void 0, void 0, function* () {
                try {
                    if (err) {
                        reject(err);
                    }
                    else {
                        if (auth) {
                            const ldapData = yield this.ldapSearchGroup(userInput,(err, results) => {
                                if (err) {
                                    reject(err);
                                }
                                else {
                                    resolve(results);
                                }
                            });
                            resolve(ldapData);
                        }
                        else {
                            reject(301);
                        }
                    }
                }
                catch(error){
                    console.log('errorldaplogin',error);
                    reject(301);
                }
                }));
            });
        });
    }
    ldapSearchGroup(userInput) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                const sAMAccountName = userInput.username + this.domainAccount;
                this.configureLdapConnection['username'] = sAMAccountName;
                this.configureLdapConnection['password'] = userInput.password;
                Authorization.activeDirectory = new activedirectory2_1.default(this.configureLdapConnection);
                Authorization.activeDirectory.getGroupMembershipForUser(sAMAccountName, (err, groups) => __awaiter(this, void 0, void 0, function* () {
                try {    
                    if (err) {
                        reject(err);
                    }
                    else {
                        if (!groups) {
                            reject(302);
                        }
                        else {
                            let response = {
                                groups: [],
                                user: {}
                            };
                            
                            for (const group of groups) {
                                if (response.groups.indexOf(group['cn']) < 0) {
                                    response.groups.push(group['cn']);
                                }
                                this.sleep(500);
                            }
                            response.user = yield this.ldapSearchUser(userInput,(err, results) => {
                                if (err) {
                                    reject(err);
                                }
                                else {
                                    resolve(results);
                                }
                            });
                            resolve(response);
                        }
                    }
                }
                catch(error){
                    console.log('error group',error);
                    reject(error);
                }
                }));
            });
        });
    }
    ldapSearchUser(userInput) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                const sAMAccountName = userInput.username + this.domainAccount;
                this.configureLdapConnection['username'] = sAMAccountName;
                this.configureLdapConnection['password'] = userInput.password;
                Authorization.activeDirectory = new activedirectory2_1.default(this.configureLdapConnection);
                Authorization.activeDirectory.findUser(sAMAccountName, (err, results) => {
                try {
                    if (err) {
                        reject(err);
                    }
                    else {
                        resolve(results);
                    }
                }
                catch(error){
                    console.log('error search', error);
                    reject(error);
                }
                });
            });
        });
    }
    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
}

exports.Authorization = Authorization;
