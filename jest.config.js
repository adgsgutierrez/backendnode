// module.exports = {
//     roots: ['<rootDir>/src'],
//     transform: {
//       '^.+\\.tsx?$': 'ts-jest',
//     },
//     testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$',
//     moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
//   }

module.exports = {
  roots: ['<rootDir>/src'],
  globals: {
      "ts-jest": {
          tsConfig: "tsconfig.json"
      }
  },
  testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  transform: {
      "^.+\\.(ts|tsx)$": "ts-jest"
  },
  collectCoverage: true,
  coverageDirectory: './.coverage',
};
