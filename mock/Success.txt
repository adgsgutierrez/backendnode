# extended LDIF
#
# LDAPv3
# base <DC=nh,DC=inet> with scope subtree
# filter: sAMAccountName=YANTOLINE
# requesting: memberOf 
#

# Yonathan Antoline, Temporales, Usuarios, Oriente, nh.inet
dn: CN=Yonathan Antoline,OU=Temporales,OU=Usuarios,OU=Oriente,DC=nh,DC=inet
memberOf: CN=ToolBox,OU=GRUPOS NUEVOS,OU=Grupos,OU=Telecom,DC=nh,DC=inet
memberOf: CN=CSC_TecnicoEECC,OU=GRUPOS NUEVOS,OU=Grupos,OU=Telecom,DC=nh,DC=in
 et
memberOf: CN=TOADirect,OU=Grupos,OU=Aplicaciones,OU=Telecom,DC=nh,DC=inet

# search reference
ref: ldap://DomainDnsZones.nh.inet/DC=DomainDnsZones,DC=nh,DC=inet

# search reference
ref: ldap://ForestDnsZones.nh.inet/DC=ForestDnsZones,DC=nh,DC=inet

# search reference
ref: ldap://nh.inet/CN=Configuration,DC=nh,DC=inet

# search result
search: 2
result: 0 Success

# numResponses: 5
# numEntries: 1
# numReferences: 3
