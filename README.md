# Autenticación con LDAP desde Node JS

Este componente se encarga de realizar las validaciones de Autenticación hacia el LDAP interno de Telefónica

## Como Funciona

```
    1. Se solicita token de autenticación con usuario y contraseña encriptado
    2. Se envia el usuario y contraseña que se requiere junto con el token de autenticacion obtenido
```

### Como iniciar

El proyecto esta bajo arquitectura de NodeJS con Framework TypeScript

### Pre-requisitos

Se debe contar con las siguientes herramientas para el desarrollo

```
    1. NodeJS versión 10.15.3 o superior
    2. Npm versión 6.4.1 o superior

```

### Instalación

Para poder correr el proyecto se deben seguir los siguientes pasos

1. Descargar repositorio

```
git clone https://git.nh.inet/gitea/AutoGestion/chatbot-tecnico_ldap-login.git
```

2. Para descargar e instalar las dependencias se debe correr el siguiente comando

```
npm run install 

npm run instal --only=dev
```

### Iniciar el componente

Dentro de la carpeta del proyecto 

```
npm run start:app 
```

### Donde queda la compilación

Los transpilados necesarios se encuentran en el directorio /dist


### Como consumir los servicios

Para ello debe utilizar herramientas como postman para realizar el flujo

## Servicio de token de autorizacion

Se debe realizar la peticion de la siguiente manera

```
Método : POST
URL    : HOST:3001/api/v1/authorization
BODY   : 
{
	"user" : { ENCRIPTADO DE 'us3rL0g1n4uth$<AÑOMESDIA en formato AAAAMMDD>' },
	"password" : { ENCRIPTADO DE 'p4ssw0rdL0g1n4uth$<AÑOMESDIA en formato AAAAMMDD>'},
	"hostDestiny" : {ENCRIPTADO DE  <HOST>:3001/api/v1/authorization/ldap}
}
```

## Servicio de login de autorizacion

Se debe realizar la peticion de la siguiente manera

```
Método : POST
URL    : HOST:3001/api/v1/authorization/ldap
HEADER : {
    authorization : <token obtenido en la autorizacion>
}
BODY   : 
{
	"user" : { ENCRIPTADO DE usuario de ldap },
	"password" : { ENCRIPTADO DE contraseña de ldap },
}

```
## Encripción solicitada

Se describirá el algoritmo que se utiliza para la encripción de los parámetros

1. Cadena se pasa a Base 64
2. Se agrega un caracter al inicio de la cadena
3. Se codifica la cadena a Base 64

4. Se agrega un caracter al inicio de la cadena
5. Se codifica la cadena a Base 64

6. Se agrega un caracter al inicio de la cadena
7. Se codifica la cadena a Base 64

8. Se envia la cadena

### Autores

```
    2. Digital Experience Everis ( ChatBot e Integraciones ) 
    2. Tren Autogestión y DevOps  
```